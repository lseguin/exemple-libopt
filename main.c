/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lseguin <lseguin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/05 15:50:33 by lseguin           #+#    #+#             */
/*   Updated: 2013/12/06 18:01:13 by lseguin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>
#include "libft.h"
#include "libopt.h"

#define USER_DEFAULT		"lseguin"

layout_opt		*construct_layout()
{
	layout_opt		*l_o;

	l_o = ft_initlayoutoption(8);
	ft_addoption(l_o, ft_createoption('U', 1, 0));
	ft_addoption(l_o, ft_createoption('v', 1, 1));
	ft_addoption(l_o, ft_createoption('b', 0, 2));
	ft_addoption(l_o, ft_createoption('c', 1, 1));
	ft_addoption(l_o, ft_createoption('I', 2, 4));
	ft_addoption(l_o, ft_createoption('L', 0, 0));
	ft_addoption(l_o, ft_createoption('x', 0, 0));
	ft_addoption(l_o, ft_createoption('m', 0, 0));
	return (l_o);
}

int		main(int argc, char **argv)
{
	layout_opt	*layout_option;
	int			ret;
	int			i;

	if (argc == 1)
	{
		printf("usage: exemple [-U arg0 [arg1 [arg2 [...]]]] [-v arg0] [-b [arg0 [arg1]]] [-c arg0] [-I arg0 arg1 [arg3 [arg4]]] [-Lxm]\n");
		return (0);
	}
	layout_option = construct_layout();
	if ((ret = ft_readoption(layout_option, argc, argv)) == -1)
	{
		printf("error\n");
		return (0);
	}
	ft_rmoption(ret, &argc, argv);
	ft_debugoption(layout_option);
	if (argc > 1)
	{
		printf("argv: \n");
		i = 1;
		while (i < argc)
			printf("    %s\n", argv[i++]);
	}
	return (0);
}
