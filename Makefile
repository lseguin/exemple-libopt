# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lseguin <lseguin@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/12/05 16:12:13 by lseguin           #+#    #+#              #
#    Updated: 2013/12/06 17:24:41 by lseguin          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME=exemple

SRC=main.c

OBJ=$(SRC:.c=.o)

all: buildlib $(NAME) finish

$(NAME): $(SRC)
	gcc -Wall -Werror -Wextra -o $(NAME) -I ./libopt/includes -I ./libft/includes -L ./libopt -L ./libft -lopt -lft $(SRC)

buildlib:
	@(cd libft && $(MAKE))
	@(cd libopt && $(MAKE))

finish:
	@(echo "[\033[32m$(NAME)\033[00m]\tis created!")
