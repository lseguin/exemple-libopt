/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_addoption.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Lionel <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/05 23:33:06 by Lionel            #+#    #+#             */
/*   Updated: 2013/12/06 00:13:29 by Lionel           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libopt.h"

void		ft_addoption(layout_opt *l_opt, option *opt)
{
	int		i;

	i = 0;
	while (l_opt->options[i])
		i++;
	l_opt->options[i] = opt;
}
