/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_initlayoutoption.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Lionel <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/05 23:20:05 by Lionel            #+#    #+#             */
/*   Updated: 2013/12/06 13:01:15 by Lionel           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libopt.h"

layout_opt		*ft_initlayoutoption(int nb_option)
{
	layout_opt		*lo;

	lo = (layout_opt *) malloc(sizeof (layout_opt));
	lo->options = (option **) ft_memalloc(sizeof (option *) * (nb_option + 1));
	return (lo);
}
