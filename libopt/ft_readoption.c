/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_readoption.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lseguin <lseguin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/05 17:06:13 by lseguin           #+#    #+#             */
/*   Updated: 2013/12/06 16:54:17 by lseguin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "libopt.h"

int		ft_readargs(option *opt, int argc, char **argv)
{
	int		i;

	i = 0;
	while (i < argc
			&& !ft_isoption(argv[i])
			&& (opt->arg_max > i || opt->arg_max == 0))
		i++;
	if (i < opt->arg_min)
		return (-1);
	if (i > 0)
		opt->args = (char **) ft_memalloc(sizeof (char *) * (i + 1));
	i = 0;
	while (i < argc
			&& !ft_isoption(argv[i])
			&& (opt->arg_max > i || opt->arg_max == 0))
	{
		opt->args[i] = argv[i];
		i++;
	}
	return (i);
}

int		ft_activeoption(layout_opt *l_opt, char *option_list)
{
	option		*opt;

	while (*option_list)
	{
		if (!(opt = ft_getoption(l_opt, *option_list++)))
			return (-1);
		if (opt->arg_min > 0)
			return (-1);
		opt->exist = 1;
	}
	return (1);
}

int		ft_prereadargs(layout_opt *l_opt, int argc, char **argv)
{
	int		n;
	option	*opt;

	n = 0;
	if (!(opt = ft_getoption(l_opt, *(argv[0] + 1))))
		return (-1);
	if ((opt->arg_min != 0 || opt->arg_max != 0))
	{
		if ((n = ft_readargs(opt, argc - 1, argv + 1)) == -1)
			return (-1);
	}
	opt->exist = 1;
	return (n);
}

int		ft_readoption(layout_opt *l_opt, int argc, char **argv)
{
	int		i;
	int		n;

	i = 1;
	while (i < argc && ft_isoption(argv[i]))
	{
		if (ft_strlen(argv[i] + 1) == 1)
		{
			if ((n = ft_prereadargs(l_opt, argc - i, argv + i)) == -1)
				return (-1);
			i += n;
		}
		else
		{
			if (ft_activeoption(l_opt, argv[i] + 1) == -1)
				return (-1);
		}
		i++;
	}
	return (i - 1);
}
