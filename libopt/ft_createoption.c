/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_createoption.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Lionel <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/05 23:46:29 by Lionel            #+#    #+#             */
/*   Updated: 2013/12/06 03:29:34 by Lionel           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libopt.h"

option		*ft_createoption(char option_n, int arg_min, int arg_max)
{
	option		*op;

	op = (option *) malloc(sizeof (option));
	op->option = option_n;
	op->arg_min = arg_min;
	op->arg_max = arg_max;
	op->exist = 0;
	op->args = NULL;
	return (op);
}
