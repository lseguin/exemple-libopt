/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libopt.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lseguin <lseguin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/05 17:41:47 by lseguin           #+#    #+#             */
/*   Updated: 2013/12/06 16:11:11 by lseguin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __LIBOPT_H__
# define __LIBOPT_H__
# include "libft.h"

typedef struct			opt
{
	char				option;
	int					arg_min;
	int					arg_max;
	char				**args;
	int					exist;
}						option;

typedef struct			l_option
{
	option				**options;
}						layout_opt;

int				ft_isoption(char *str);
int				ft_issetoption(layout_opt *l_opt, char option);
int				ft_readoption(layout_opt *l_opt, int argc, char **argv);
void			ft_rmoption(int readoption, int *argc, char **argv);
layout_opt		*ft_initlayoutoption(int nb_option);
void			ft_addoption(layout_opt *l_opt, option *opt);
option			*ft_createoption(char option, int arg_min, int arg_max);
option			*ft_getoption(layout_opt *l_opt, char option);
void			ft_debugoption(layout_opt *l_o);


#endif /* !__LIBOPT_H__ */
