/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getoption.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Lionel <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/06 01:38:26 by Lionel            #+#    #+#             */
/*   Updated: 2013/12/06 03:29:47 by Lionel           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libopt.h"

option		*ft_getoption(layout_opt *l_opt, char option_c)
{
	option		**options;

	options = l_opt->options;
	while (*options)
	{
		if ((*options)->option == option_c)
			return (*options);
		options++;
	}
	return (NULL);
}
