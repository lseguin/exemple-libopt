/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_optionisset.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lseguin <lseguin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/05 17:12:29 by lseguin           #+#    #+#             */
/*   Updated: 2013/12/06 02:02:52 by Lionel           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libopt.h"

int		ft_issetoption(layout_opt *l_opt, char option)
{
	if (l_opt && option)
		return (1);
	return (0);
}
