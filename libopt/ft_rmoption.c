/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_removeoption.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lseguin <lseguin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/05 19:16:50 by lseguin           #+#    #+#             */
/*   Updated: 2013/12/06 16:55:12 by lseguin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libopt.h"
#include <stdio.h>

void	ft_rmoption(int readoption, int *argc, char **argv)
{
	int		i;

	i = 1;
	while (i < *argc - readoption)
	{
		argv[i] = argv[i + readoption];
		i++;
	}
	*argc -= readoption;
}
