/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_debugoption.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Lionel <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/06 12:15:04 by Lionel            #+#    #+#             */
/*   Updated: 2013/12/06 16:03:38 by lseguin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libopt.h"
#include <stdio.h>

void		ft_debugoption(layout_opt *l_o)
{
	option		**options;
	option		*op;
	char		**args;

	options = l_o->options;
	while (*options)
	{
		op = *options;
		if (op->exist)
		{
			printf("option \x1B[32m%c\x1B[00m is active\n", op->option);
			if (op->args)
			{
				args = op->args;
				while (*args)
					printf("    \x1B[33m%s\x1B[00m\n", *(args++));
			}
			else
				printf("    \x1B[31m#no args\x1B[00m\n");
		}
		options++;
	}
}
