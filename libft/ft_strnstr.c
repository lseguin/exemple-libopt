/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lseguin <lseguin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 08:39:12 by lseguin           #+#    #+#             */
/*   Updated: 2013/11/25 08:10:53 by lseguin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *s1, const char *s2, size_t n)
{
	size_t	s2_len;

	if (*s2 == '\0')
		return ((char *) s1);
	s2_len = ft_strlen(s2);
	while (*s1 && n > 0 && s2_len <= n)
	{
		if (ft_strncmp(s1, s2, s2_len) == 0)
			return ((char *) s1);
		s1++;
		n--;
	}
	return (NULL);
}
