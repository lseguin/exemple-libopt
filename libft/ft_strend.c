/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strend.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lseguin <lseguin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/02 14:28:20 by lseguin           #+#    #+#             */
/*   Updated: 2013/12/02 15:47:26 by lseguin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strend(const char *s, char end)
{
	char	*str;
	int		i;
	int		n;

	i = 0;
	n = 0;
	while (s[i] != end && s[i])
		i++;
	if (!(str = ft_strnew(i)))
		return (NULL);
	while (n < i)
	{
		str[n] = s[n];
		n++;
	}
	return (str);
}
