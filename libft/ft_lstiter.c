/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstiter.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lseguin <lseguin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/28 15:39:42 by lseguin           #+#    #+#             */
/*   Updated: 2013/12/02 14:46:42 by lseguin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void			ft_lstiter(t_list *alst, void (*f)(t_list *))
{
	t_list		*next;

	if (alst)
	{
		next = alst->next;
		f(alst);
		ft_lstiter(next, f);
	}
}
