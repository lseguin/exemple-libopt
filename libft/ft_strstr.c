/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lseguin <lseguin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 08:40:29 by lseguin           #+#    #+#             */
/*   Updated: 2013/11/29 19:06:31 by lseguin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *s1, const char *s2)
{
	size_t	s2_len;

	if (*s2 == '\0')
		return ((char *) s1);
	s2_len = ft_strlen(s2);
	while (*s1)
	{
		if (ft_strncmp(s1, s2, s2_len) == 0)
			return ((char *) s1);
		s1++;
	}
	return (NULL);
}
