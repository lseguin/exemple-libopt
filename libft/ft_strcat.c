/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lseguin <lseguin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 08:36:02 by lseguin           #+#    #+#             */
/*   Updated: 2013/12/02 14:53:21 by lseguin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char		*ft_strcat(char *dest, const char *src)
{
	char	*start;

	start = dest;
	while (*dest)
		dest++;
	while ((*dest++ = *src++));
	return (start);
}
