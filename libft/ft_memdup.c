/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lseguin <lseguin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/02 14:49:44 by lseguin           #+#    #+#             */
/*   Updated: 2013/12/02 14:49:47 by lseguin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		*ft_memdup(const void *data, size_t n)
{
	char	*dst;

	dst = malloc(sizeof (char) * n);
	ft_memcpy(dst, data, n);
	return (dst);
}
