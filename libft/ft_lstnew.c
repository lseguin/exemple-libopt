/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lseguin <lseguin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/25 10:04:32 by lseguin           #+#    #+#             */
/*   Updated: 2013/12/02 14:47:55 by lseguin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list		*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*l;

	l = (t_list *) malloc(sizeof (t_list));
	l->content = (void *) content;
	l->content_size = content_size;
	l->next = NULL;
	return (l);
}
