/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lseguin <lseguin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 08:38:30 by lseguin           #+#    #+#             */
/*   Updated: 2013/12/02 14:56:25 by lseguin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strncat(char *dst, const char *src, size_t n)
{
	char		*d;
	const char	*s;

	d = dst;
	s = src;
	while (*d != 0)
		d++;
	while (n--)
	{
		if ((*d = *s++) == 0)
			break ;
		d++;
	}
	*d = 0;
	return (dst);
}
