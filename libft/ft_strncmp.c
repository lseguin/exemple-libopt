/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lseguin <lseguin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 08:38:44 by lseguin           #+#    #+#             */
/*   Updated: 2013/12/02 16:12:57 by lseguin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strncmp(const char *s1, const char *s2, size_t n)
{
	while (n-- && *s1 && *s2)
	{
		if (*s1++ != *s2++)
			return ((unsigned char) *(s1 - 1) - (unsigned char) *(s2 - 1));
		if (n == 0)
			return (0);
	}
	return ((unsigned char) *s1 - (unsigned char) *s2);
}
