/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strend_dest.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lseguin <lseguin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/02 15:47:44 by lseguin           #+#    #+#             */
/*   Updated: 2013/12/02 17:01:05 by lseguin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int			ft_strend_dest(const char *s, char end, char **dest)
{
	char	*str;
	int		i;
	int		n;
	int		flag;

	i = 0;
	n = 0;
	flag = 1;
	while (s[i] != end && s[i])
		i++;
	if (s[i] == '\0')
		flag = 0;
	if (!(str = ft_strnew(i)))
		return (0);
	while (n < i)
	{
		str[n] = s[n];
		n++;
	}
	*dest = str;
	return (flag);
}
