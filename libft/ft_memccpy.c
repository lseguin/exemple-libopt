/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lseguin <lseguin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 10:05:46 by lseguin           #+#    #+#             */
/*   Updated: 2013/12/02 14:38:00 by lseguin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	*ft_memccpy(void *s1, const void *s2, int c, size_t n)
{
	unsigned char			u_c;
	unsigned char			*u_s1;
	const unsigned char		*u_s2;

	u_s1 = (unsigned char *) s1;
	u_s2 = (const unsigned char *) s2;
	while (n--)
	{
		u_c = *u_s2;
		*u_s1++ = *u_s2++;
		if ((unsigned char) c == u_c)
			return (u_s1);
	}
	return (NULL);
}
