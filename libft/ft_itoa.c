/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lseguin <lseguin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 13:06:39 by lseguin           #+#    #+#             */
/*   Updated: 2013/12/02 15:20:55 by lseguin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int			ft_countnumber(int n)
{
	int		i;

	i = 0;
	if (n < 0)
		n = -1 * n;
	if (n == 0)
		return (1);
	while (n > 0)
	{
		i++;
		n /= 10;
	}
	return (i);
}

int			ft_power(int n, int p)
{
	int		res;

	res = n;
	while (p--)
		res *= n;
	return (res);
}

char	*ft_itoa(int n)
{
	char	buff[] = "00000000000";
	char	*p;
	int		neg;

	neg = 0;
	p = buff + ft_strlen(buff);
	if (n == 0)
		*--p = '0';
	else
	{
		if (n < 0)
			neg = 1;
		while (n != 0)
		{
			if (n < 0)
				*--p = '0' - (n % 10);
			else
				*--p = '0' + (n % 10);
			n /= 10;
		}
		if (neg)
			*--p = '-';
	}
	return (ft_strdup(p));
}
