/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lseguin <lseguin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 08:31:33 by lseguin           #+#    #+#             */
/*   Updated: 2013/12/02 14:48:37 by lseguin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		*ft_memchr(const void *s, int c, size_t n)
{
	const unsigned char		*p;

	p = s;
	while (n--)
	{
		if (*p++ == (unsigned char) c) {
			return ((void *) (p - 1));
		}
	}
	return (0);
}
