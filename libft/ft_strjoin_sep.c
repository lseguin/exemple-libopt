/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin_sep.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lseguin <lseguin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/02 14:53:45 by lseguin           #+#    #+#             */
/*   Updated: 2013/12/02 14:53:47 by lseguin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strjoin_sep(char const *s1, char const *s2, char c)
{
	char	*str;
	int		len;

	len = ft_strlen(s1);
	if (!(str = ft_strnew(len + ft_strlen(s2) + 1)))
		return (NULL);
	str = ft_strcat(str, s1);
	str[len] = c;
	return (ft_strcat(str, s2));
}
