/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lseguin <lseguin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 10:51:55 by lseguin           #+#    #+#             */
/*   Updated: 2013/11/25 15:32:16 by lseguin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int			ft_iswhitespace(int c)
{
	if (c == ' ' || c == '\n' || c == '\t')
		return (1);
	return (0);
}

char		*ft_strtrim(char const *s)
{
	int		len;
	int		endchar;
	int		startchar;

	if (!s)
		return (NULL);
	endchar = 0;
	startchar = 0;
	len = ft_strlen(s);
	while (ft_iswhitespace(s[len - 1 - endchar]))
		endchar++;
	while (ft_iswhitespace(s[startchar]))
		startchar++;
	if ((len - startchar - endchar) <= 0)
		return (ft_strnew(0));
	return (ft_strsub(s, startchar, (size_t) (len - startchar - endchar)));
}
