/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_timespccmp.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lseguin <lseguin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/29 16:28:43 by lseguin           #+#    #+#             */
/*   Updated: 2013/11/30 19:30:41 by lseguin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <time.h>

int		ft_timespccmp(struct timespec t1, struct timespec t2)
{
	if ((t1.tv_sec < t2.tv_sec)
			|| (t1.tv_sec == t2.tv_sec && t1.tv_nsec < t2.tv_nsec))
		return (1);
	else if ((t1.tv_sec < t2.tv_sec)
			|| (t1.tv_sec == t2.tv_sec && t1.tv_nsec > t2.tv_nsec))
		return (-1);
	return (0);
}
