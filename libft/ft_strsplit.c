/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lseguin <lseguin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 12:39:44 by lseguin           #+#    #+#             */
/*   Updated: 2013/12/02 14:59:01 by lseguin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_freesplit(char ***tab)
{
	while (**tab)
		free(**tab++);
	free(*tab);
}

int			ft_nbsplit(char const *s, char c)
{
	char	lastchar;
	int		i;

	lastchar = c;
	i = 0;
	while (*s)
	{
		if (lastchar == c && *s != c)
			i++;
		lastchar = *s++;
	}
	return (i);
}

char		**ft_strsplit(char const *s, char c)
{
	int		nbcase;
	int		i;
	char	**tab;

	i = 0;
	nbcase = ft_nbsplit(s, c);
	tab = (char **) ft_memalloc((nbcase + 1) * sizeof (tab));
	while (i < nbcase)
	{
		while (*s == c)
			s++;
		if (!(tab[i] = ft_strend(s, c)))
		{
			ft_freesplit(&tab);
			return (NULL);
		}
		while (*s != c)
			s++;
		i++;
	}
	return (tab);
}
