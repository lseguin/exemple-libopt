/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lseguin <lseguin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 08:34:48 by lseguin           #+#    #+#             */
/*   Updated: 2013/11/25 16:07:45 by lseguin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void					*ft_memcpy(void *s1, const void *s2, size_t n)
{
	unsigned char		*u8_s1;
	const unsigned char	*u8_s2;

	u8_s1 = (unsigned char *) s1;
	u8_s2 = (const unsigned char *) s2;
	while (n--)
		*u8_s1++ = *u8_s2++;
	return (s1);
}
