/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strndup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lseguin <lseguin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/30 18:34:24 by lseguin           #+#    #+#             */
/*   Updated: 2013/12/02 14:58:34 by lseguin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strndup(const char *s, size_t n)
{
	char	*result;
	size_t	len;

	len = ft_strlen(s);
	len = (n < len ? n : len);
	result = (char *) ft_strnew(len);
	if (!result)
		return (0);
	result[len] = '\0';
	return ((char *) ft_memcpy(result, s, len));
}
