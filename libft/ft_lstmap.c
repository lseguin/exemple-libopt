/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lseguin <lseguin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/28 15:38:15 by lseguin           #+#    #+#             */
/*   Updated: 2013/11/28 15:39:54 by lseguin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list		*ft_lstmap(t_list *lst, t_list * (*f)(t_list *elem))
{
	t_list	*startlist;
	t_list	*l;
	t_list	*tmp;

	tmp = f(lst);
	l = ft_lstnew(tmp->content, tmp->content_size);
	startlist = l;
	while (lst)
	{
		tmp = (lst->next ? f(lst->next) : NULL);
		if (tmp)
			l->next = ft_lstnew(tmp->content, tmp->content_size);
		else
			l->next = NULL;
		l = l->next;
		lst = lst->next;
	}
	return (startlist);
}
