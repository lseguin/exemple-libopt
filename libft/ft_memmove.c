/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lseguin <lseguin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 15:40:56 by lseguin           #+#    #+#             */
/*   Updated: 2013/11/23 16:42:49 by lseguin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "libft.h"

void	*ft_memmove(void *s1, const void *s2, size_t n)
{
	unsigned char			u_buffer[n];

	ft_memcpy((void *) u_buffer, s2, n);
	ft_memcpy(s1, (const void *) u_buffer, n);
	return (s1);
}
