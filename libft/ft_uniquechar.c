/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_uniquechar.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lseguin <lseguin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/05 17:16:00 by lseguin           #+#    #+#             */
/*   Updated: 2013/12/05 17:17:00 by lseguin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_count_uniquechar(char *str)
{
	int		i;
	int		n;
	int		find;
	int		nboption;

	i = 0;
	nboption = 0;
	while (str[i])
	{
		find = 0;
		n = 0;
		while (n < i && find == 0)
		{
			if (str[i] == str[n])
				find = 1;
			n++;
		}
		if (find == 0)
			nboption += 1;
		i++;
	}
	return (nboption);
}

char	*ft_uniquechar(char *str)
{
	int		i;
	int		n;
	int		find;
	int		numc;
	char	*s;

	numc = 0;
	s = ft_strnew(ft_count_uniquechar(str));
	i = 0;
	while (str[i])
	{
		find = 0;
		n = 0;
		while (n < i && find == 0)
		{
			if (str[i] == str[n])
				find = 1;
			n++;
		}
		if (find == 0)
			s[numc++] = str[i];
		i++;
	}
	return (s);
}
